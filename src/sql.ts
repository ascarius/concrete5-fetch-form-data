import { isNull, isString, isDate } from 'util';

export function formatTable(table: string) {
  return formatColumn(table);
}

export function formatColumn(column: string) {
  return `\`${column}\``;
}

export function formatValue(value: any) {
  if (isNull(value)) return 'NULL';
  if (isString(value)) return `"${value}"`;
  if (isDate(value))
    return `"${value
      .toJSON()
      .slice(0, 19)
      .replace('T', ' ')}"`;
  return value;
}

export function format(data: Map<string, Map<string, any>>): string {
  return inserts(data) + '\n' + updates(data);
}

function inserts(data: Map<string, Map<string, any>>) {
  return Array.from(data.keys())
    .map(table => insert(table, Array.from(data.get(table).values())))
    .join('\n');
}

function updates(data: Map<string, Map<string, any>>) {
  const forms = data.get('ExpressForms');
  const form = Array.from(forms.values())[0];
  return update('ExpressEntities', { default_view_form_id: form.id, default_edit_form_id: form.id }, { id: form.entity_id });
}

function insert(table: string, rows: any[]) {
  if (rows.length === 0) {
    return `-- Nothing to insert in ${table}\n`;
  }

  const first = rows[0];
  const columns = Object.keys(first);
  const values = rows.map(
    row =>
      `  (${Object.values(row)
        .map(formatValue)
        .join(',')})`
  );
  return `INSERT INTO ${formatTable(table)} (${columns.map(formatColumn).join(',')}) VALUES\n${values.join(',\n')};\n`;
}

function update(table: string, values: any, criteria: { [column: string]: string }) {
  return `UPDATE ${formatTable(table)} SET ${equals(values).join(', ')} WHERE ${equals(criteria).join(' AND ')};`;
}

export function select(table: string, criteria: any) {
  return `SELECT * from ${formatTable(table)} WHERE ${equals(criteria).join(' AND ')};`;
}

function equals(criteria: { [column: string]: string }) {
  return Object.keys(criteria).map(column => equal(column, criteria[column]));
}

function equal(column: string, value: any) {
  return `${formatColumn(column)} = ${formatValue(value)}`;
}
