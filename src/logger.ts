export enum LogLevel {
  Debug,
  Info,
  Notice,
  Warn,
  Error,
}

export class Logger {
  constructor(public level = LogLevel.Debug) {}

  debug(message: string, ...args: any[]) {
    if (this.level <= LogLevel.Debug) {
      console.debug(`[Debug] ${message}`, ...args);
    }
  }

  info(message: string, ...args: any[]) {
    if (this.level <= LogLevel.Info) {
      console.info(`[Info] ${message}`, ...args);
    }
  }

  notice(message: string, ...args: any[]) {
    if (this.level <= LogLevel.Notice) {
      console.log(`[Notice] ${message}`, ...args);
    }
  }

  warn(message: string, ...args: any[]) {
    if (this.level <= LogLevel.Warn) {
      console.warn(`[Warn] ${message}`, ...args);
    }
  }

  error(message: string, ...args: any[]) {
    if (this.level <= LogLevel.Error) {
      console.error(`[Error] ${message}`, ...args);
    }
  }
}