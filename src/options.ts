import * as commandLineArgs from 'command-line-args';
import { ConnectionConfig } from 'mysql';

export interface Options {
  form: string;
  verbose: boolean;
  db: ConnectionConfig;
}

export async function getOptions() {
  const args = commandLineArgs([
    { name: 'form', alias: 'f', type: String, defaultOption: true },
    { name: 'verbose', alias: 'v', type: Boolean },
  ]) as Options;
  const db = require(`${__dirname}/../db.json`);

  if (!args.form) {
    throw new Error('--form is required');
  }

  return {
    form: args.form,
    verbose: args.verbose,
    db,
  };
}
