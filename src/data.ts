import { DB } from './db';
import { Options } from './options';
import { getPrimaryKey, getPrimaryValue, getRelationKey, getRelations } from './schema';

export async function fetch(options: Options, db: DB) {
  return new Data(db, options.form).run();
}

class Data {
  private data: Map<string, Map<string, any>>;
  private resolved: Set<string>;

  constructor(private db: DB, private formId: string) {}

  public async run() {
    try {
      this.data = new Map<string, Map<string, any>>();
      this.resolved = new Set<string>();
      this.db.connect();

      const forms = await this.db.select('ExpressForms', { id: this.formId });
      if (forms.length < 1) {
        throw new Error('Form not found');
      }

      this.add('ExpressForms', forms);
      await this.fetch('ExpressForms', forms);

      this.db.end();

      return this.data;
    } catch (error) {
      this.db.end();
      throw error;
    }
  }

  private async fetch(table: string, items: any[]) {
    for (const item of items) {
      for (const relation of getRelations()) {
        if (relation.to !== table || this.isResolved(table, item, relation)) {
          continue;
        }

        const column = relation.owns ? relation.foreignKey : getPrimaryKey(relation.from);
        const value = relation.owns ? getPrimaryValue(table, item) : item[relation.foreignKey];
        const related = await this.db.select(relation.from, { [column]: value });
        this.add(relation.from, related);
        this.setResolved(table, item, relation);
        await this.fetch(relation.from, related);
      }
    }
  }

  private isResolved(table: string, item: any, relation: any) {
    return this.resolved.has(this.getResolveKey(table, item, relation));
  }

  private setResolved(table: string, item: any, relation: any) {
    this.resolved.add(this.getResolveKey(table, item, relation));
  }

  private getResolveKey(table: string, item: any, relation: any) {
    return `${table}/${getPrimaryValue(table, item)}/${getRelationKey(relation)}`;
  }

  private add(table: string, items: any[]) {
    if (!this.data.get(table)) {
      this.data.set(table, new Map<string, any>());
    }

    for (const item of items) {
      this.data.get(table).set(getPrimaryValue(table, item), item);
    }
  }
}
