interface Table {
  primaryKey: string;
  primaryKeys?: string[];
}

interface Relation {
  from: string;
  foreignKey: string;
  owns: boolean;
  to: string;
}

const tables: { [name: string]: Table } = {
  AttributeKeys: { primaryKey: 'akID' },
  AttributeValues: { primaryKey: 'avID' },
  ExpressAttributeKeys: { primaryKey: 'akID' },
  ExpressFormFieldSetAssociationControls: { primaryKey: 'id' },
  ExpressFormFieldSetAttributeKeyControls: { primaryKey: 'id' },
  ExpressFormFieldSetControls: { primaryKey: 'id' },
  ExpressFormFieldSets: { primaryKey: 'id' },
  ExpressForms: { primaryKey: 'id' },
  ExpressEntities: { primaryKey: 'id' },
  ExpressEntityEntries: { primaryKey: 'exEntryID' },
  ExpressEntityEntryAssociations: { primaryKey: 'id' },
  ExpressEntityEntryAttributeValues: { primaryKey: 'avID' /* Not primary, but unique */ },
  ExpressEntityAssociations: { primaryKey: 'id' },
};

const relations: Relation[] = [
  {
    from: 'ExpressEntities',
    foreignKey: 'default_view_form_id',
    to: 'ExpressForms',
    owns: true,
  },
  {
    from: 'ExpressEntityEntries',
    foreignKey: 'exEntryEntityID',
    to: 'ExpressEntities',
    owns: true,
  },
  {
    from: 'ExpressEntityEntryAttributeValues',
    foreignKey: 'exEntryID',
    to: 'ExpressEntityEntries',
    owns: true,
  },
  {
    from: 'AttributeKeys',
    foreignKey: 'akID',
    to: 'ExpressEntityEntryAttributeValues',
    owns: false,
  },
  {
    from: 'ExpressFormFieldSets',
    foreignKey: 'form_id',
    to: 'ExpressForms',
    owns: true,
  },
  {
    from: 'ExpressFormFieldSetControls',
    foreignKey: 'field_set_id',
    to: 'ExpressFormFieldSets',
    owns: true,
  },
  {
    from: 'ExpressFormFieldSetAttributeKeyControls',
    foreignKey: 'id',
    to: 'ExpressFormFieldSetControls',
    owns: true,
  },
  {
    from: 'ExpressFormFieldSetAssociationControls',
    foreignKey: 'id',
    to: 'ExpressFormFieldSetControls',
    owns: true,
  },
  {
    from: 'AttributeKeys',
    foreignKey: 'akID',
    to: 'ExpressFormFieldSetAttributeKeyControls',
    owns: false,
  },
  {
    from: 'AttributeValues',
    foreignKey: 'akID',
    to: 'AttributeKeys',
    owns: true,
  },
  {
    from: 'ExpressAttributeKeys',
    foreignKey: 'akID',
    to: 'AttributeKeys',
    owns: false,
  },
  {
    from: 'ExpressEntityEntryAttributeValues',
    foreignKey: 'akID',
    to: 'AttributeKeys',
    owns: false,
  },
  {
    from: 'ExpressEntityAssociations',
    foreignKey: 'association_id',
    to: 'ExpressFormFieldSetAssociationControls',
    owns: false,
  },
  {
    from: 'ExpressEntityEntryAssociations',
    foreignKey: 'association_id',
    to: 'ExpressEntityAssociations',
    owns: true,
  },
  {
    from: 'ExpressEntityEntryAssociations',
    foreignKey: 'exEntryID',
    to: 'ExpressEntityEntries',
    owns: true,
  },
  {
    from: 'ExpressEntityAssociations',
    foreignKey: 'association_id',
    to: 'ExpressEntityEntryAssociations',
    owns: false,
  },
];

export function getRelations() {
  return relations;
}

export function getRelationKey(relation: Relation) {
  const { from, foreignKey, to, owns } = relation;
  return owns ? `${from}->${foreignKey}->${to}` : `${from}<-${foreignKey}<-${to}`;
}

export function getPrimaryKey(table: string): string {
  if (!tables[table]) {
    throw new Error(`Table "${table}" not found`)
  }
  return tables[table].primaryKey;
}

export function getPrimaryValue(table: string, item: any) {
  return item[getPrimaryKey(table)];
}
