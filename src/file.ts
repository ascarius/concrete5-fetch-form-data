import { promises } from 'fs';
import { Options } from './options';

const writeFile = promises.writeFile;

export async function write(content: string, options: Options): Promise<string> {
  const fileName = `${process.cwd()}/form-${options.form}.sql`;
  await writeFile(fileName, content, 'utf8');
  return fileName;
}
