import { fetch } from './data';
import { write } from './file';
import { Logger, LogLevel } from './logger';
import { getOptions } from './options';
import { format } from './sql';
import { DB } from './db';

(async function main() {
  let options;
  try {
    options = await getOptions();
    const logger = new Logger(options.verbose ? LogLevel.Info : LogLevel.Notice);
    const db = new DB(options.db, logger);
    const data = await fetch(options, db);
    const content = await format(data);
    const fileName = await write(content, options);
    logger.notice(`Migration file created at ${fileName}`);
  } catch (error) {
    if (options && options.verbose) {
      console.error('Error:', error);
    } else {
      console.error(`Error: ${error.message}`);
    }
  }
})();
