import { Connection, ConnectionConfig, createConnection } from 'mysql';
import { Logger } from './logger';
import { select } from './sql';

export class DB {
  private connection: Connection;

  constructor(config: ConnectionConfig, private logger: Logger) {
    this.connection = createConnection(config);
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.connection.connect(error => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }

  end() {
    return new Promise((resolve, reject) => {
      this.connection.end(error => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }

  query(sql: string): Promise<any[]> {
    this.logger.info(`[DB] Query: ${sql}`)
    return new Promise((resolve, reject) => {
      this.connection.query(sql, (error, results: any[]) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      });
    });
  }

  select(table: string, criteria: any) {
    return this.query(select(table, criteria));
  }
}
